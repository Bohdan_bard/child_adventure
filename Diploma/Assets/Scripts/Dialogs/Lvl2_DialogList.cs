﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lvl2_DialogList : MonoBehaviour
{
    int[] dialogCount = { 3, 5, 7 };

    string[] dialogArray = { "Привiт! Сьогоднi ми з тобою помалюємо", "Я взяла жовту, синю i зелену фарбу", "Давай почнемо, я хочу намалювати сонечко",
    "Яку фарбу ми будемо використовувати?",
        "Тепер я хочу намалювати травичку", "Яку фарбу ми будемо використовувати?" , "В кiнцi давай домалюємо рiчечку!","Яку фарбу ми будемо використовувати?" };

    string positiveAnswer = "Дивись як красиво вийшло!";

    string negativeAnswer = "Менi здається, нам потрiбна iнша фарба";
    int prelvlCnt = 3;
    public int PreLvlCnt
    {
        get
        {
            return prelvlCnt;
        }
        private set { }
    }
    public string Replica(int index)
    { return dialogArray[index]; }

    public string Replica(bool answer)
    {
        if (answer)
        { return positiveAnswer; }
        else { return negativeAnswer; }
    }

    public int Level1Counter(int index)
    {
        return dialogCount[index];
    }
}
