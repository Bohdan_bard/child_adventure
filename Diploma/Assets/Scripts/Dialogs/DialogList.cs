﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogList : MonoBehaviour
{
    
    int[] dialogCount = { 2, 4, 7 };
    
    string[] dialogArray = { "О нi! На дорогу впало дерево  ", "Треба щось зробити!","Нумо подивимось, що у нас є ",
                            "От халепа! Поки я бiгла порвався рюкзак", "Може ми зможемо його полагодити тим що у нас є?",
                               "Дивись! Шпакiвня впала на землю", "Я думаю ми зможемо прибити її назад", "Як думаєш, що нам допоможе?"};

    int prelvlCnt = 3;
    public int PreLvlCnt { get {
            return prelvlCnt;
        }
        private set { }
    }

#if UNITY_ANDROID
    string[] preLevelText = {"Вiтаю! Щоб перейти до наступного кроку клацни по екрану! ",
        "Щоразу коли прочитаєш дiалог натисни, щоб перейти далi!", "Якщо ти готовий, давай вiдправимося в подорож"  };
#endif

#if UNITY_STANDALONE
     string[] preLevelText = {"ПРИВЕТ! ЧТОБ ПЕРЕЙТИ К СЛЕДУЮЩЕМУ ШАГУ КЛИКНИ МЫШКОЙ!",
        "КАЖДЫЙ РАЗ КОГДА ПРОЧИТАЕШЬ ДИАЛОГ КЛИКНИ, ЧТОБ ПЕРЕЙТИ ДАЛЬШЕ!", "ЕСЛИ ТЫ ГОТОВ, ДАВАЙ ОТПРАВИМСЯ К ПУТЕШЕСТВИЯМ"  };
#endif

    string positiveAnswer = "Ура! Все вийшло! Йдемо далi";

    string negativeAnswer = "Не думаю що це допоможе";

    public string PreLevelTextReturner(int index)
    {

        return preLevelText[index];
    }

    public string Level1(int index)
    { return dialogArray[index]; }

    public string Level1(bool answer)
    {if (answer)
        { return positiveAnswer; }
        else { return negativeAnswer; }
        }

    public int Level1Counter(int index)
    {
        return dialogCount[index];
    }
}
