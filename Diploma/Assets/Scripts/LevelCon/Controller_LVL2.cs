﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Controller_LVL2 : MonoBehaviour
{
    

    public GameObject textWindow;
    public Animator planeAnim;
    public Text textComp;
    public GameObject menu;
    public GameObject[] lvlObjects;

  
    private AudioController audioDialog ;



    int lvlCnt = 0;
    int counter = 0;


     Lvl2_DialogList dialogList;


    private void Awake()
    {
        dialogList = GetComponent<Lvl2_DialogList>();
    }
    void Start()
    {
        audioDialog = GetComponent<AudioController>();

        planeAnim.SetBool("gameState", true);
        StartOfLevel();
    }

    void StartOfLevel()
    {

       
        StartCoroutine("TextAppearing");
    }

    IEnumerator TextAppearing()
    {

      
        yield return new WaitForSeconds(3f);

        textWindow.SetActive(true);
        for (; counter <= dialogList.Level1Counter(lvlCnt); counter++)
        {

            textComp.text = dialogList.Replica(counter);
            audioDialog.AudioPlay(counter);
            yield return StartCoroutine(WaitForClick.waitForKeyPress(KeyCode.Mouse0) );//waitForKeyPress(KeyCode.Mouse0);
            audioDialog.AudioStop();
        }
        lvlCnt++;

        textWindow.SetActive(false);
        menu.SetActive(true);
    }

    public void CheckForResult(int checker)
    {

        if (checker == lvlCnt)
        {
            StartCoroutine("Correct");
        }
        else { StartCoroutine("WrongAction"); }
    }

    IEnumerator Correct()
    {
        audioDialog.AudioPlay(true);
        menu.SetActive(false);
        lvlObjects[lvlCnt - 1].GetComponent<Animation>().Play();
      
        textWindow.SetActive(true);
        textComp.text = dialogList.Replica(true);



        yield return StartCoroutine(WaitForClick.waitForKeyPress(KeyCode.Mouse0));

        if (lvlCnt == 3)
        {
            planeAnim.SetBool("gameState", false);
            yield return new WaitForSeconds(3f);
            SceneManager.LoadScene("MainMenu");
        }
        else
        {

            StartOfLevel();
        }
    }

    IEnumerator WrongAction()
    {
        audioDialog.AudioPlay(false);
        menu.SetActive(false);
        textWindow.SetActive(true);
        textComp.text = dialogList.Replica(false);
        yield return new WaitForSeconds(3f);
        menu.SetActive(true);
        textWindow.SetActive(false);

    }
}
