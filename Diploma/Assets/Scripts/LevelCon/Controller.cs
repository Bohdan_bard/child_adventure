﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Controller : MonoBehaviour
{
    [SerializeField]
    private Animator girlAnim = null;
    [SerializeField]
    private Animator animPlayerMovement = null;
    [SerializeField]
    private GameObject textWindow = null;
    [SerializeField]
    private Animator planeAnim = null;
    [SerializeField]
    private Text textComp = null;
    [SerializeField]
    private GameObject menu = null;
    [SerializeField]
    private GameObject[] lvlObjects = null;
    [SerializeField]
    private AudioController audioDialog = null;
  
    int lvlCnt = 0;
    int counter = 0;

    public  DialogList dialogList;

    void Start()
    {

        planeAnim.SetInteger("lvlCnt", lvlCnt);

        StartOfLevel();
    }

    void StartOfLevel()
    {
       
        for (int i = 0; i < 3; i++)
        {
            if (lvlCnt == i) { lvlObjects[i].SetActive(true); }
            else { lvlObjects[i].SetActive(false); }
        }
   
        StartCoroutine("TextAppearing");
    }

    IEnumerator TextAppearing()
    {
       
        animPlayerMovement.SetBool("isRun", false);
        yield return new WaitForSeconds(3f);
        girlAnim.SetBool("isIdle", true);
        textWindow.SetActive(true);
        for (; counter <= dialogList.Level1Counter(lvlCnt); counter++)
        {
         
            textComp.text = dialogList.Level1(counter);
            audioDialog.AudioPlay(counter);
            yield return StartCoroutine(WaitForClick.waitForKeyPress(KeyCode.Mouse0) );//waitForKeyPress(KeyCode.Mouse0);
            audioDialog.AudioStop();
        }
        lvlCnt++;
       
        textWindow.SetActive(false);
        menu.SetActive(true);
    }

    public void CheckForResult(int checker)
    {
      
        if (checker == lvlCnt)
        {
            StartCoroutine("Correct");
        }
        else { StartCoroutine("WrongAction"); }
    }

    IEnumerator Correct()
    {
        audioDialog.AudioPlay(true);
        menu.SetActive(false);
      lvlObjects[lvlCnt-1].GetComponent<Animation>().Play();
        animPlayerMovement.SetBool("isRun", true);
        textWindow.SetActive(true);
        textComp.text = dialogList.Level1(true);
  
        girlAnim.SetBool("isIdle", false);
        planeAnim.SetInteger("lvlCnt", lvlCnt);
        yield return new WaitForSeconds(3f);
        textWindow.SetActive(false);
        if (lvlCnt == 3)
        {
            SceneManager.LoadScene("MainMenu");
        }
        else {
         
            StartOfLevel();
        }
    }
   
    IEnumerator WrongAction()
    {
        audioDialog.AudioPlay(false);
        menu.SetActive(false);
        textWindow.SetActive(true);
        textComp.text =dialogList.Level1(false);
        yield return new WaitForSeconds(3f);
        menu.SetActive(true);
        textWindow.SetActive(false);

    }
}
