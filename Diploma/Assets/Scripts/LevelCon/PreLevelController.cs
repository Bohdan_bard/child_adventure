﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PreLevelController : MonoBehaviour
{
    public  DialogList dialogList;
    public Text textField;
    public Animator planeAnim;
    [SerializeField]
    private AudioClip[] _audioClips = null;

    private AudioSource _audioSource;
    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        StartCoroutine("Dialog");
    }

    IEnumerator Dialog()
    {for (int i = 0; i < dialogList.PreLvlCnt; i++)
        {
            _audioSource.clip = _audioClips[i];
            _audioSource.Play();
            textField.text = dialogList.PreLevelTextReturner(i);
            yield return StartCoroutine(WaitForClick.waitForKeyPress( KeyCode.Mouse0));
            _audioSource.Stop();
        }
        planeAnim.SetBool("firstEnter", true);
        yield return new WaitForSeconds(3f);
        planeAnim.SetBool("firstEnter", false);
        SceneManager.LoadScene("Level1");
    }

   
}
