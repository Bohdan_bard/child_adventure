﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetLevelMusicVolume : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<AudioSource>().volume = PlayerPrefs.GetFloat("MusicVolume", 1f);
    }

    
}
