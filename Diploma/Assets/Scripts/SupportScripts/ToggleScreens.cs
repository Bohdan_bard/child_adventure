﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleScreens : MonoBehaviour
{
    [SerializeField]
    private GameObject _screenToOff = null;
    [SerializeField]
    private GameObject _screenToOn = null;
   

    // Update is called once per frame
    void Update()
    {
        StartCoroutine(Toggle()); 
    }

    private IEnumerator Toggle()
    {
        yield return StartCoroutine(WaitForClick.waitForKeyPress(KeyCode.Mouse0));
     
        _screenToOn.SetActive(true);
        _screenToOff.SetActive(false);
    }
}
