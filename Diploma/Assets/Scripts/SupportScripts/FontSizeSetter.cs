﻿using UnityEngine.UI;
using UnityEngine;

public class FontSizeSetter : MonoBehaviour
{
    [SerializeField]
    private Text _textField = null;

    // Start is called before the first frame update
    void Start()
    {
        _textField.fontSize = PlayerPrefs.GetInt("FontSize", 50);
    }


}
