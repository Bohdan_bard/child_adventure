﻿using UnityEngine.UI;
using UnityEngine;

public class PauseButtonScript : MonoBehaviour
{
    [SerializeField]
    private Button _pauseButton = null;

    private PauseMenuScript _pauseMenuScript = null;

    private void Start()
    {
        _pauseMenuScript = transform.GetChild(0).GetComponent<PauseMenuScript>();
        _pauseButton.onClick.AddListener(_pauseMenuScript.StartPause);    
    }


}
