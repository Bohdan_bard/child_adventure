﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitForClick : MonoBehaviour
{
    public static IEnumerator waitForKeyPress(KeyCode key)
    {

        bool done = false;
        while (!done) // essentially a "while true", but with a bool to break out naturally
        {


            if (Input.GetKeyDown(key)&& !PauseMenuScript.isPause)
            {
                            done = true; // breaks the loop
            }

             if (Input.touchCount > PlayerPrefs.GetInt("Enter", -1)&& !PauseMenuScript.isPause)
            {
                done = true;
                PlayerPrefs.SetInt("Enter", Input.touchCount);
            }


                yield return null; // wait until next frame, then continue execution from here (loop continues)
         
        }

        // now this function returns
    }
}
