﻿using UnityEngine.UI;
using UnityEngine;

public class MusicVolumeSetter : MonoBehaviour
{
    [SerializeField]
    private Slider _musicSlider = null;

    private AudioSource _audioSource;

    // Start is called before the first frame update
    void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        _musicSlider.value = PlayerPrefs.GetFloat("MusicVolume", 1f);
        _musicSlider.onValueChanged.AddListener(SaveVolume);
    }

    private void SaveVolume(float value)
    {
        PlayerPrefs.SetFloat("MusicVolume", value);
        _audioSource.volume = value;
    }
}
