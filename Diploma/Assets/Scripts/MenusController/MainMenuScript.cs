﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuScript : MonoBehaviour
{

    [SerializeField]
    private GameObject levelChoosenMenu = null;
    [SerializeField]
    private GameObject startMenu = null;
    [SerializeField]
    private GameObject settingsMenu = null;
    [SerializeField]
    Slider volumeSlider = null;


    private int checkForEnter;

    private void Awake()
    {
        checkForEnter = PlayerPrefs.GetInt("Enter", 0);
        if (checkForEnter == 0)
        {
            SceneManager.LoadScene("PreLevel");
            PlayerPrefs.SetInt("Enter", 1);
        }
    }

    public void LoadLevel(int lvlNumber)
    {
      if (lvlNumber == 0)
        {
            SceneManager.LoadScene("Level1");
        }
        else {
            SceneManager.LoadScene("Level2");
        } 
    }

    public void QuitGame()
    { Application.Quit(); }

    public void LevelChoosen()
    {
        startMenu.SetActive(false);
        levelChoosenMenu.SetActive(true);
    }

    public void Continue()
    {
        SceneManager.LoadScene(PlayerPrefs.GetString("SavedScene", "Level1") );
    }

    public void SettingsMenu()
    {
        if (settingsMenu.activeInHierarchy)
        {
            PlayerPrefs.SetFloat("Volume", volumeSlider.value);
          
            startMenu.SetActive(true);
            settingsMenu.SetActive(false);
        }
        else {
          
            volumeSlider.value = PlayerPrefs.GetFloat("Volume", 1f);
            startMenu.SetActive(false);
            settingsMenu.SetActive(true);
      GetComponent<SettingsMenuController>().SetActiveButton();
        }
    }

    public void FontSizeChange(int fontSize)
    {
        PlayerPrefs.SetInt("FontSize", fontSize);
    }
}
