﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsMenuController : MonoBehaviour
{
    private const int SmallFont = 40;
    private const int MiddleFont = 50;
    private const int BigFont = 60;


    [SerializeField]
    private Button _smallFontButton = null;
    [SerializeField]
    private Button _middleFontButton = null;
    [SerializeField]
    private Button _bigFontButton = null;

    private Image _smallButtonImage ;
    private Image _middleButtonImage ;
    private Image _bigButtonImage;

    private Color _activeColor = new Color32() { r = 255, g = 192, b = 67, a = 255 };

    private void Start()
    {
        _smallFontButton.onClick.AddListener(SetActiveButton);
        _middleFontButton.onClick.AddListener(SetActiveButton);
        _bigFontButton.onClick.AddListener(SetActiveButton);

        _smallButtonImage = _smallFontButton.GetComponent<Image>();
        _middleButtonImage = _middleFontButton.GetComponent<Image>();
        _bigButtonImage = _bigFontButton.GetComponent<Image>();

        
    }

   public  void SetActiveButton()
    {
        var fontSize = PlayerPrefs.GetInt("FontSize", 50);
        switch (fontSize)
        {
            case SmallFont:
                _smallButtonImage.color = _activeColor;
                _middleButtonImage.color = Color.white;
                _bigButtonImage.color = Color.white;
                break;
            case MiddleFont:
                _smallButtonImage.color = Color.white;
                _middleButtonImage.color = _activeColor;
                _bigButtonImage.color = Color.white;
                break;
            case BigFont:
                _smallButtonImage.color = Color.white;
                _middleButtonImage.color = Color.white;
                _bigButtonImage.color = _activeColor;
                break;
        }
    }
}
