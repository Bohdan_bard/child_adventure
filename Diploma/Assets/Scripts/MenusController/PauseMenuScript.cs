﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public  class PauseMenuScript : MonoBehaviour
{
    public static bool isPause = false;

    public void StartPause()
    {
        isPause = true;
        Time.timeScale = 0f;
        gameObject.SetActive(true);
        transform.GetComponentInParent<Canvas>().sortingOrder = 1;
    }

    public void StopPause()
    {
       
        Time.timeScale = 1f;
        gameObject.SetActive(false);
        GetComponentInParent<Canvas>().sortingOrder = 0;
        isPause = false;
    }

    public void Exit()
    {
        PlayerPrefs.SetString("SavedScene", SceneManager.GetActiveScene().name);
        isPause = false;
        Time.timeScale = 1f;
        SceneManager.LoadScene("MainMenu");
    }
}
