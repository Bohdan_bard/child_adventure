﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    public AudioClip[] dialogVoice;

    public AudioClip positiveAnswer;
    public AudioClip negativeAnswer;

    public AudioSource player;

    private void Start()
    {
        player.volume = PlayerPrefs.GetFloat("Volume", 1f);
    }

    public void AudioPlay(int index)
    {
        player.clip = dialogVoice[index];
        player.Play();
    }

    public void AudioStop()
    {
        player.Stop();
    }

    public void AudioPlay(bool answer)
    {
        if (answer)
        {
            player.clip = positiveAnswer;
            player.Play();
        }
        else {
            player.clip = negativeAnswer;
            player.Play();
        }
    }
}
